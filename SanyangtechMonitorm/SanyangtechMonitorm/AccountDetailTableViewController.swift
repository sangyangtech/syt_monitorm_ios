//
//  AccountDetailTableViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/21.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class AccountDetailTableViewController: UITableViewController {

    var account: AccountDTO = AccountDTO()
    
    @IBOutlet var loginIdTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var warrantyDateTextField: UITextField!
    @IBOutlet var modifyDateLabel: UILabel!
    @IBOutlet var createLabel: UILabel!
    @IBOutlet var statusSegmented: UISegmentedControl!
    @IBOutlet var isStopSwitch: UISwitch!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var commentTextView: UITextView!
    
    private var datePicker: UIDatePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginIdTextField.text = account.LoginId
        passwordTextField.text = account.Password
        warrantyDateTextField.text = account.WarrantyDate
        modifyDateLabel.text = account.ModifyDate
        createLabel.text = account.CreateDate
        statusSegmented.selectedSegmentIndex = account.Status
        isStopSwitch.isOn = account.Stop == 1
        commentTextView.text = account.Comment
        
        submitButton.layer.cornerRadius = 5;
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
        
        view.addGestureRecognizer(tapGesture)
        warrantyDateTextField.inputView = datePicker
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "登出", style: UIBarButtonItem.Style.done, target: self, action: #selector(logoutAction(sender:)))
        
      
        self.navigationItem.rightBarButtonItem = rightButton
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
        view.endEditing(true) 
    }
    
    @objc func logoutAction(sender: UIBarButtonItem!) {
        let memberRepo = MemberRepository()
        memberRepo.DeleteMember()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy/MM/dd"
        warrantyDateTextField.text = dateFormater.string(from: datePicker.date)
        view.endEditing(true)
        
    }

    
    @IBAction func submit(sender: UIButton){
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd HH:mm:ss"
        
        account.LoginId = loginIdTextField.text!
        account.Password = passwordTextField.text!
        account.Stop = isStopSwitch.isOn ? 1 : 0
        account.WarrantyDate = warrantyDateTextField.text!
        account.Status = statusSegmented.selectedSegmentIndex
        account.ModifyDate = dateFormatter.string(from: date)
        account.Comment = commentTextView.text
        
        let manager = SangyangTechCloud()
    
        DispatchQueue.main.async {
            let vcu = ViewControllerUtils()
            vcu.startActivityIndicatorAnimating(uiView: self.view)
            
            DispatchQueue.main.async {
                
                manager.UpdateDataToGoogleSheet(account: self.account)
                
                let alertController = UIAlertController(
                    title: "會員資料更新",
                    message: "成功",
                    preferredStyle: .alert)
                
                // 建立[確認]按鈕
                let okAction = UIAlertAction(
                    title: "確認",
                    style: .default,
                    handler: {
                        (action: UIAlertAction!) -> Void in
                        print("按下確認後，閉包裡的動作")
                })
                alertController.addAction(okAction)
                
                // 顯示提示框
                self.present(
                    alertController,
                    animated: true,
                    completion: nil)
                
                
                DispatchQueue.main.async {
                    vcu.stopActivityIndicatorAnimating()
                }
                
            }
        }
        
       
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
    
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        header.textLabel?.frame = header.frame
    
    }
    
    // MARK: - Table view data source

    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
