//
//  AccountTableViewCell.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/21.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    
    @IBOutlet var warrantyDateLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
