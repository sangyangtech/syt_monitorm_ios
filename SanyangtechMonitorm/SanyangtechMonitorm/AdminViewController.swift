//
//  ViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/1/31.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit


class AdminViewController: UITableViewController {
    
    var accounts: [AccountDTO]? = nil
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let manager = SangyangTechCloud()
        accounts = manager.GetAccounts(isFilterAdmin: true)
        
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "登出", style: UIBarButtonItem.Style.done, target: self, action: #selector(logoutAction(sender:)))
        
        
        self.navigationItem.rightBarButtonItem = rightButton
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func logoutAction(sender: UIBarButtonItem!) {
        let memberRepo = MemberRepository()
        memberRepo.DeleteMember()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return accounts!.count
    }
    
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        let cellIdentifier = "datacell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AccountTableViewCell
        
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy/MM/dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        let oldDate = dateFormatterGet.date(from: accounts![indexPath.row].WarrantyDate)
        let newDate = dateFormatterPrint.string(from: oldDate!)
        cell.warrantyDateLabel.text = newDate
        
        cell.textLabel?.text = accounts![indexPath.row].LoginId
     return cell
     }
    
    
    
    override var prefersStatusBarHidden: Bool{
        return false
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        if segue.identifier == "showAccountDetail" {
            
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationController = segue.destination as! AccountDetailTableViewController
                
                destinationController.account = accounts![indexPath.row]
                
            }
        }
     }
 
    
}
