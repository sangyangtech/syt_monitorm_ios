//
//  ExtendedWarrantyViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/15.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//


import UIKit
import MessageUI

class ExtendedWarrantyViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var ExtendButton: UIButton!
    @IBOutlet weak var CallEmailButton: UIButton!
    
    @IBOutlet weak var CallPhoneButton: UIButton!
    @IBOutlet weak var ExplainLabel: UILabel!
    
    @IBOutlet weak var WarrantyDateLabel: UILabel!
    var resourceMap: Dictionary<String, String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let manager = SangyangTechCloud();
        let memberRepo = MemberRepository()
        let member = memberRepo.GetMember()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy/MM/dd"
        resourceMap = manager.GetResources()
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        let oldDate = dateFormatterGet.date(from: (member?.warrantyPeriod)!)
        let newDate = dateFormatterPrint.string(from: oldDate!)
        WarrantyDateLabel.text = newDate
        ExplainLabel.numberOfLines = 0
        ExplainLabel.text = "欲延長保固請點選「延長保固」按鈕，將會有專人與您聯繫。"
        
        ExtendButton.layer.cornerRadius = 5
        CallEmailButton.layer.cornerRadius = 5
        CallPhoneButton.layer.cornerRadius = 5
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "登出", style: UIBarButtonItem.Style.done, target: self, action: #selector(logoutAction(sender:)))
        
        self.navigationItem.rightBarButtonItem = rightButton
        
    }
    
    @objc func logoutAction(sender: UIBarButtonItem!) {
        let memberRepo = MemberRepository()
        memberRepo.DeleteMember()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func CallServiceByMail(sender: UIButton){
        let manager = SangyangTechCloud()
        let config = manager.GetConfigs()
        
        let mailCompese = MFMailComposeViewController()
        mailCompese.mailComposeDelegate = self
        mailCompese.setToRecipients([config["cs_email"]!])
        mailCompese.setSubject("延長保固期限")
        mailCompese.setMessageBody("延長保固期限", isHTML: false)
        
        if MFMailComposeViewController.canSendMail(){
            self.present(mailCompese, animated: true, completion: nil)
        } else {
            print("無法寄信")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func CallServiceByPhone(sender: UIButton){
        let manager = SangyangTechCloud()
        let config = manager.GetConfigs()
        
        
        let url: NSURL = URL(string: "tel://\(config["cs_tel"]!)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    
    
    @IBAction func ExtendedWarranty(sender: UIButton){
        
        DispatchQueue.main.async {
            let vcu = ViewControllerUtils()
            vcu.startActivityIndicatorAnimating(uiView: self.view)
            
            DispatchQueue.main.async {
                
                //let manager = SangyangTechCloud()
                
                let mailSubjectTemplate = self.resourceMap!["extend_notify_title"]
                let mailContentTemplate = self.resourceMap!["extend_notify_content"]
                
                let memberRepo = MemberRepository()
                let member = memberRepo.GetMember()
                
                
                let subject = mailSubjectTemplate!
                    .replacingOccurrences(of: "{Account}", with: "帳號:" + member!.loginId!, options: .literal, range: nil)
                
                let content = mailContentTemplate!
                    .replacingOccurrences(of: "{Account}", with: "帳號:" + member!.loginId!, options: .literal, range: nil)
                
                
                let notificationManager = NotificationManager()
                notificationManager.SendMail(subject: subject, content: content)
                
                notificationManager.SendLineMessage(value1: subject, value2: "", value3: "")
                
                let alertController = UIAlertController(
                    title: "完成通知",
                    message: "已通知專員，將與您聯繫。",
                    preferredStyle: .alert)
                
                // 建立[確認]按鈕
                let okAction = UIAlertAction(
                    title: "確認",
                    style: .default,
                    handler: {
                        (action: UIAlertAction!) -> Void in
                        print("按下確認後，閉包裡的動作")
                })
                alertController.addAction(okAction)
                
                // 顯示提示框
                self.present(
                    alertController,
                    animated: true,
                    completion: nil)
                DispatchQueue.main.async {
                    vcu.stopActivityIndicatorAnimating()
                }
                
            }
        }
        
        
        
    }
}
