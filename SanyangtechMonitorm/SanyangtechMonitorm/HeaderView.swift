//
//  File.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/17.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var WarryantDateLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    private func commitInit(){
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        addSubview(containerView)
        
        let memberRepo = MemberRepository()
        let member = memberRepo.GetMember()
        
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy/MM/dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        let oldDate = dateFormatterGet.date(from: (member?.warrantyPeriod)!)
        let newDate = dateFormatterPrint.string(from: oldDate!)
        WarryantDateLabel.text = newDate
    }
}
