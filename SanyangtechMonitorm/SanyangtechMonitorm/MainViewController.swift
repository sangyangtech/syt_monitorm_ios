//
//  MainViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/21.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class MainViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let memberRepo = MemberRepository()
        let member = memberRepo.GetMember()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if(member != nil){
            
            let dateFormatter = DateFormatter()
            let currentDateTime = Date()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            
            let manager = SangyangTechCloud()
            let accountData: AccountDTO? = manager.GetAccountInfo(id: (member?.loginId)!, pwd: (member?.password)!)
            
            let warrantyPeriodDate = dateFormatter.date(from: (accountData?.WarrantyDate)!)
            
            memberRepo.UpdateWarrantyDate(warrantyDate: (accountData?.WarrantyDate)!)
            
            if(member!.role == "user" && warrantyPeriodDate! < currentDateTime){
                let extendedWarrantyViewController = storyBoard.instantiateViewController(withIdentifier: "ExtendedWarranty") as! ExtendedWarrantyViewController
                //self.present(extendedWarrantyViewController, animated: false, completion: nil)
                self.pushViewController(extendedWarrantyViewController, animated: true)
            
            }else if(member!.role == "user" && member!.faultReasonType < 0) {
                
                let userRepairViewController = storyBoard.instantiateViewController(withIdentifier: "UserRepair") as! UserRepairViewController
                //self.present(userRepairViewController, animated: false, completion: nil)
                self.pushViewController(userRepairViewController, animated: true)
                
                
            } else if(member!.role == "user" && member!.faultReasonType >= 0) {
                
                let troubleshootingViewController = storyBoard.instantiateViewController(withIdentifier: "Troubleshooting") as! TroubleshootingViewController
                //self.present(troubleshootingViewController, animated: false, completion: nil)
                self.pushViewController(troubleshootingViewController, animated: true)
                
            } else if (member!.role == "admin") {
                
                let adminViewController = storyBoard.instantiateViewController(withIdentifier: "Admin") as! AdminViewController
                
                self.pushViewController(adminViewController, animated: true)
               
            } else {
                let loginViewController = storyBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
                
                self.pushViewController(loginViewController, animated: true)
                
            }
        }
        else {
            let loginViewController = storyBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
            
            self.pushViewController(loginViewController, animated: true)
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
