//
//  MemberRepositry.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/13.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import CoreData
import UIKit

public class MemberRepository {
    
    public func ResetMember(loginId: String, password: String, role: String, warrantyDate: String){
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            
            let request: NSFetchRequest<MemberInfoMO> = MemberInfoMO.fetchRequest()
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                let memberInfos: [MemberInfoMO]? = try context.fetch(request)
                if let member = memberInfos
                {
                    if member.count > 0
                    {
                        context.delete(member.first!)
                    }
                    
                    let newMember = MemberInfoMO(context: context)
                    newMember.loginId = loginId
                    newMember.password = password
                    newMember.role = role
                    newMember.faultReasonType = -1
                    newMember.warrantyPeriod = warrantyDate
                    appDelegate.saveContext()
                }
                
            }catch{
                print(error)
            }
            
        }
        
    }
    
    public func DeleteMember(){
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            
            let request: NSFetchRequest<MemberInfoMO> = MemberInfoMO.fetchRequest()
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                let memberInfos: [MemberInfoMO]? = try context.fetch(request)
                if let member = memberInfos
                {
                    if member.count > 0
                    {
                        context.delete(member.first!)
                    }
                    appDelegate.saveContext()
                }
                
            }catch{
                print(error)
            }
        }
        
    }
    
    public func GetMember() -> MemberInfoMO? {
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            
            let request: NSFetchRequest<MemberInfoMO> = MemberInfoMO.fetchRequest()
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                let memberInfos: [MemberInfoMO]? = try context.fetch(request)
                if let members = memberInfos
                {
                    if members.count > 0
                    {
                        return members.first!
                    }
                    else
                    {
                        return nil
                    }
                }
                
            }catch{
                print(error)
                return nil
            }
            
        }
        return nil
    }
    
    public func UpdateFaultReason(reasonType:Int, reasonTypeText: String, reasonRemark: String) {
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            
            let request: NSFetchRequest<MemberInfoMO> = MemberInfoMO.fetchRequest()
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                let memberInfos: [MemberInfoMO]? = try context.fetch(request)
                if let members = memberInfos
                {
                    members.first?.setValue(reasonType, forKey: "faultReasonType")
                    members.first?.setValue(reasonRemark, forKey: "faultReasonRemark")
                    members.first?.setValue(reasonTypeText, forKey: "faultReasonTypeText")
                    appDelegate.saveContext()
                }
                
            }catch{
                print(error)
                
            }
        }
    }
    
    public func UpdateWarrantyDate(warrantyDate: String) {
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            
            let request: NSFetchRequest<MemberInfoMO> = MemberInfoMO.fetchRequest()
            let context = appDelegate.persistentContainer.viewContext
            
            do {
                let memberInfos: [MemberInfoMO]? = try context.fetch(request)
                if let members = memberInfos
                {
                    members.first?.setValue(warrantyDate, forKey: "warrantyPeriod")
                    appDelegate.saveContext()
                }
                
            }catch{
                print(error)
                
            }
        }
    }
}
