//
//  NotificationManager.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/2.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//
import UIKit
import MessageUI

public class NotificationManager: UIViewController, MFMailComposeViewControllerDelegate{
    
    public func SendLineMessage(value1: String, value2: String, value3: String){
       
        let manager = SangyangTechCloud()
        let config = manager.GetConfigs()
        let parameters = ["value1" : value1, "value2" : value2, "value3" : value3]
    
        
        let url = URL(string: config["ifttt_url"]!)!
        //let url = URL(string: "https://maker.ifttt.com/trigger/testtrigger/with/key/gES3D7EWYMbYF0orKEeYKwFyX8VHIVfrtrpHR76ePf2")!
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                   
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
 
        
    }
    
  
    
    public func SendMail(subject: String, content: String){
        
        let manager = SangyangTechCloud()
        let config = manager.GetConfigs()
        //let resource = manager.GetResources()
        
        
        let json: [String: Any] = ["to": config["receivers"] as Any,
                                   "subject": subject,
                                   "content": content]
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        
        // create post request
        let url = URL(string: "https://script.google.com/macros/s/AKfycby698eZOrce868Utzd768ziTdTL2vKml_MSj3Ou0fjzk3d-HGGi/exec")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
        }
        
        task.resume()
        
        
//        print("=======================")
//        print(UInt32(config["smtp_port"]!)!)
//        print("=======================")
//
//        let smtpSession = MCOSMTPSession()
//        smtpSession.hostname = config["smtp_url"]
//        smtpSession.username = config["sender"]
//        smtpSession.password = config["sender_pwd"]
//        smtpSession.port = UInt32(config["smtp_port"]!)!
//        smtpSession.authType = MCOAuthType.saslPlain
//        smtpSession.connectionType = MCOConnectionType.TLS
//        smtpSession.connectionLogger = {(connectionID, type, data) in
//            if data != nil {
//                if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
//                    NSLog("Connectionlogger: \(string)")
//                }
//            }
//        }
//
//        let builder = MCOMessageBuilder()
//        builder.header.to = [MCOAddress(displayName: "客服人員", mailbox: config["receivers"])]
//        builder.header.from = MCOAddress(displayName: "Service", mailbox: config["sender"])
//        builder.header.subject = resource[subjectKey]
//        builder.htmlBody = resource[contentKey]
//
//        let rfc822Data = builder.data()
//        let sendOperation = smtpSession.sendOperation(with: rfc822Data)
//        sendOperation!.start { (error) -> Void in
//            if (error != nil) {
//                NSLog("Error sending email: \(error)")
//            } else {
//                NSLog("Successfully sent email!")
//            }
//        }
        
    }
    
   
}
