//
//  MyLib.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/1/31.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//


public class SangyangTechCloud {
    
    var sheetIdMap: Dictionary<String, String>
    var sheetRangeMap: Dictionary<String, String>
    init(){
        sheetIdMap = ["config"  : "1jzKTZojKUmy1r10VSVSvngN7RscNnoqFYynib1JROPk",
                     "resource":"1_QL5GePvswygq7labslJIwk56_745l2O8mDC_aavk8Y",
                     "faultReasonItems":"1nq7CU3EejgcGaVe9AT8_wsHpcFfGwY2jCEVA5yxVcjo",
                     "account":"1sP6ik2tJm_aZM2R0-67LHCs1oc2MAc1YwgejhD62t-M"]
        
        sheetRangeMap = ["config"  : "A2:B",
                      "resource":"A2:B",
                      "faultReasonItems":"A2:B",
                      "account":"A2:I"]
    }
    
    private func fetchDataFromGoogleSheet(sheetName: String) -> SheetResult{
        var result: SheetResult!
        
        let apiKey = "AIzaSyDPxUWHynP9YzHXqa0YIztgOn8shFttgp8"
        let rangeEncode = sheetRangeMap[sheetName]!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let semaphore = DispatchSemaphore(value: 0)
        let sheetId = sheetIdMap[sheetName]!
        //let urlStr = "https://sheets.googleapis.com/v4/spreadsheets/\(sheetId)/values/\(rangeEncode!)?dateTimeRenderOption=FORMATTED_STRING&majorDimension=ROWS&valueRenderOption=UNFORMATTED_VALUE&fields=majorDimension%2Crange%2Cvalues&key=\(apiKey)"
        
        
        let urlStr2 = "https://sheets.googleapis.com/v4/spreadsheets/\(sheetId)?includeGridData=true&ranges=\(rangeEncode!)&fields=sheets%2Fdata%2FrowData&key=\(apiKey)"
        
        print(urlStr2)
        
        let url = URL(string: urlStr2)
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            if let data = data, let sheetResult = try?
                decoder.decode(SheetResult.self, from: data)
            {
                
                result = sheetResult
                //print(sheetResult.sheets[0].data[0].rowData[0].values[0].formattedValue)
                
                semaphore.signal()
            } else {
                print("error")
            }
        }
        
        task.resume()
        semaphore.wait();
        
        return result
    }
    
    public func UpdateDataToGoogleSheet(account: AccountDTO){
        
        let json: [String: Any] = ["idx": String(account.idx),
                                   "id": account.LoginId,
                                   "pwd": account.Password,
                                   "expired": account.WarrantyDate,
                                   "isInvalid": account.Stop,
                                   "repairState": account.Status,
                                   "cDate": account.ModifyDate,
                                   "mDate": account.CreateDate,
                                   "identity": account.Role,
                                   "comment":account.Comment]
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
       
        
        // create post request
        let url = URL(string: "https://script.google.com/macros/s/AKfycbycKpCOOpNrKXPEQDbn87d62qDD19_pzzp6ZuuIqtZvZPRYAjc/exec")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
        }

        task.resume()
        
    }
    
    public func GetAccounts(isFilterAdmin: Bool) -> [AccountDTO]{
        var result = fetchDataFromGoogleSheet(sheetName: "account")
        var accounts: [AccountDTO] = [AccountDTO]()
        var idx: Int = 1
        for rowData in result.sheets[0].data[0].rowData{
            
            if(isFilterAdmin && rowData.values[0].formattedValue == "admin"){
                continue
            }
            
            let model = AccountDTO()
            model.idx = idx
            model.LoginId = rowData.values[0].formattedValue
            model.Password = rowData.values[1].formattedValue
            model.Role = rowData.values[7].formattedValue
            model.Status = Int(rowData.values[4].formattedValue)!
            model.Stop = Int(rowData.values[3].formattedValue)!
            model.WarrantyDate = rowData.values[2].formattedValue
            model.CreateDate = rowData.values[5].formattedValue
            model.ModifyDate = rowData.values[6].formattedValue
            
            if(rowData.values.count == 9){
                model.Comment = rowData.values[8].formattedValue
            }
            
            accounts.append(model)
            idx += 1
        }
        return accounts
    }
    
    public func GetAccountInfo(id: String, pwd: String) ->AccountDTO?{
        var model: AccountDTO?
        var _: [AccountDTO]
        var result = fetchDataFromGoogleSheet(sheetName: "account")
        
        //let dateFormatterGet = DateFormatter()
        //dateFormatterGet.dateFormat = "yyyy/MM/dd"
        
        //let dateFormatterPrint = DateFormatter()
        //dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        //let datessss = dateFormatterGet.date(from: "2019/2/19")
        //let bbb = dateFormatterPrint.string(from: datessss!)
        for rowData in result.sheets[0].data[0].rowData{
            
            let loginId = rowData.values[0].formattedValue;
            let password = rowData.values[1].formattedValue;
            
            if(id == loginId && pwd == password && Int(rowData.values[3].formattedValue)! == 0){
                model = AccountDTO()
                model!.LoginId = loginId
                model!.Password = password
                model!.Role = rowData.values[7].formattedValue
                model!.Status = Int(rowData.values[4].formattedValue)!
                model!.Stop = Int(rowData.values[3].formattedValue)!
                model!.WarrantyDate = rowData.values[2].formattedValue
                model!.CreateDate = rowData.values[5].formattedValue
                model!.ModifyDate = rowData.values[6].formattedValue
            }
            
            
        }
     
        
        /*
        
        if(id == "test" && pwd == "123qwe"){
            model = AccountDTO()
            model!.LoginId = "test"
            model!.Password = "123qwe"
            model!.Role = "user"
            model!.Status = 0
            model!.Stop = 0
            model!.WarrantyDate = "2019-02-25"
            model!.CreateDate = "2019-02-01"
            model!.ModifyDate = "2019-02-01"
            
        }else if(id == "admin" && pwd == "123qwe"){
            model = AccountDTO()
            model!.LoginId = "admin"
            model!.Password = "123qwe"
            model!.Role = "admin"
            model!.Status = 0
            model!.Stop = 0
            model!.WarrantyDate = "2019-01-25"
            model!.CreateDate = "2019-02-01"
            model!.ModifyDate = "2019-02-01"
        }else{
            model = nil
        }
        */
        
        return model
    }
    
    public func  GetResources() -> Dictionary<String, String>{
        
        var result = fetchDataFromGoogleSheet(sheetName: "resource")
        
        var dic: Dictionary<String, String> = Dictionary<String, String>()
        for rowData in result.sheets[0].data[0].rowData {
            dic.updateValue(rowData.values[1].formattedValue, forKey: rowData.values[0].formattedValue)
        }
        
        return dic
        
        /*
        return ["booking_notify_email_template": "b",
                "fault_resolved_notify_email_template":"",
                "fault_unresolved_notify_email_template":"",
                "login_fail_msg":"",
                "extend_matainance_msg":""]
         */
    }
    
    
    
    
    public func  GetConfigs() -> Dictionary<String, String>{
        
        var result = fetchDataFromGoogleSheet(sheetName: "config")
       
        var dic: Dictionary<String, String> = Dictionary<String, String>()
        for rowData in result.sheets[0].data[0].rowData {
            dic.updateValue(rowData.values[1].formattedValue, forKey: rowData.values[0].formattedValue)
        }
        
        return dic
        
//        return ["sender": "asimov.service@gmail.com",
//                "sender_pwd"       : "rxxcoupumpd",
//                "smtp_url"             : "smtp.gmail.com",
//                "smtp_port"        : "465",
//                "smtp_ssl_enabled" : "1",
//                "ifttt_url" : "https://maker.ifttt.com/trigger/testtrigger/with/key/gES3D7EWYMbYF0orKEeYKwFyX8VHIVfrtrpHR76ePf2",
//                //"receivers" : "tw.kchsieh@gmail.com",
//                "receivers" : "chuanghungchieh@gmail.com",
//                //"cs_email"  : "sanyang.simon@gmail.com",
//                "cs_email"  : "chuanghungchieh@gmail.com",
//                "cs_tel"    : "033663878"
//        ]
        
    }
    
    public func  GetFaultReasons() -> Dictionary<String, String>{
        
        var result = fetchDataFromGoogleSheet(sheetName: "faultReasonItems")
        
        var dic: Dictionary<String, String> = Dictionary<String, String>()
        for rowData in result.sheets[0].data[0].rowData {
            dic.updateValue(rowData.values[1].formattedValue, forKey: rowData.values[0].formattedValue)
        }
        
        return dic
    }
}

public class AccountDTO{
    var idx: Int
    var LoginId: String
    var Password: String
    var WarrantyDate: String
    var Stop: Int
    var Status: Int
    var Role: String
    var CreateDate: String
    var ModifyDate: String
    var Comment: String
    init(){
        idx = 0
        LoginId = ""
        Password = ""
        WarrantyDate = ""
        Stop = 0
        Status = 0
        Role = "user"
        CreateDate = ""
        ModifyDate = ""
        Comment = ""
        
    }
}

struct SheetResult: Codable {
    
    struct SheetData: Codable {
       var data: [RowData]
    }
    
    struct RowData: Codable{
        var rowData: [Values]
    }
    
    struct Values: Codable{
        var values: [ValuesItem]
    }
    
    struct ValuesItem: Codable{
        var formattedValue: String
    }

    var sheets: [SheetData]
    
}
