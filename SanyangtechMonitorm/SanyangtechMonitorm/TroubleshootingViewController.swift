//
//  TroubleshootingViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/14.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class TroubleshootingViewController: UIViewController {
    
    @IBOutlet weak var NoButton: UIButton!
    @IBOutlet weak var YesButton: UIButton!
    @IBOutlet weak var ProductNameLabel: UILabel!
    @IBOutlet weak var ReasonTextView: UITextView!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var resourceMap: Dictionary<String, String>?
    var member: MemberInfoMO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let manager = SangyangTechCloud();
        let memberRepo = MemberRepository()
        let member = memberRepo.GetMember()
      
        ProductNameLabel.text = member?.faultReasonTypeText
        ReasonTextView.text = member?.faultReasonRemark
        resourceMap = manager.GetResources()
        YesButton.layer.cornerRadius = 5
        NoButton.layer.cornerRadius = 5
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "登出", style: UIBarButtonItem.Style.done, target: self, action: #selector(logoutAction(sender:)))
        
        self.navigationItem.rightBarButtonItem = rightButton
        
        
    }
    
    @objc func logoutAction(sender: UIBarButtonItem!) {
        let memberRepo = MemberRepository()
        memberRepo.DeleteMember()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func startActivityIndicatorAnimating(){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true;
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5);
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        // UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func stopActivityIndicatorAnimating(){
        activityIndicator.stopAnimating()
        //UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    @IBAction func SendYesTroubleshooting(sender: UIButton){
        
        sendYesMessageConfirm()
        
        
    }
    
    
    func sendNoMessageConfirm() {
        // 建立一個提示框
        let alertController = UIAlertController(
            title: "提醒",
            message: "確認通知維修人員尚未排除嗎?",
            preferredStyle: .alert)
        
        // 建立[取消]按鈕
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        alertController.addAction(cancelAction)
        
        // 建立[送出]按鈕
        let okAction = UIAlertAction(
            title: "送出",
            style: .default,
            handler:  { action in
                self.sendNoTroubleshootingMessage()
        })
        alertController.addAction(okAction)
        
        // 顯示提示框
        self.present(
            alertController,
            animated: true,
            completion: nil)
    }
    
    func sendYesMessageConfirm() {
        // 建立一個提示框
        let alertController = UIAlertController(
            title: "提醒",
            message: "確認通知維修人員已排除嗎?",
            preferredStyle: .alert)
        
        // 建立[取消]按鈕
        let cancelAction = UIAlertAction(
            title: "取消",
            style: .cancel,
            handler: nil)
        alertController.addAction(cancelAction)
        
        // 建立[送出]按鈕
        let okAction = UIAlertAction(
            title: "送出",
            style: .default,
            handler:  { action in
                self.sendYesTroubleshootingMessage()
        })
        alertController.addAction(okAction)
        
        // 顯示提示框
        self.present(
            alertController,
            animated: true,
            completion: nil)
    }
    
    @IBAction func SendNoTroubleshooting(sender: UIButton){
        sendNoMessageConfirm()
    }
    
    func sendNoTroubleshootingMessage(){
        DispatchQueue.main.async {
            let vcu = ViewControllerUtils()
            vcu.startActivityIndicatorAnimating(uiView: self.view)
            
            DispatchQueue.main.async {
                
                let mailSubjectTemplate = self.resourceMap!["fault_unresolved_notify_title"]
                let mailContentTemplate = self.resourceMap!["fault_unresolved_notify_content"]
                
                let memberRepo = MemberRepository()
                let member = memberRepo.GetMember()
                
                
                let subject = mailSubjectTemplate!.replacingOccurrences(of: "{Product Category}", with: "安裝產品:" + self.ProductNameLabel.text! , options: .literal, range: nil)
                    .replacingOccurrences(of: "{Account}", with: "帳號:" + member!.loginId!, options: .literal, range: nil)
                
                let content = mailContentTemplate!.replacingOccurrences(of: "{Product Category}", with: "安裝產品:" + member!.faultReasonTypeText!, options: .literal, range: nil)
                    .replacingOccurrences(of: "{Account}", with:  "帳號:" + member!.loginId!, options: .literal, range: nil)
                    .replacingOccurrences(of: "{Error Reason}", with:  "故障原因:" + member!.faultReasonRemark!, options: .literal, range: nil)
                
                
                
                let notificationManager = NotificationManager()
                notificationManager.SendLineMessage(value1: content, value2: "", value3: "")
                notificationManager.SendMail(subject: subject, content: content)
                
                
                DispatchQueue.main.async {
                    self.finishNotifity(handler: nil)
                    vcu.stopActivityIndicatorAnimating()
                }
                
            }
        }
    }

    func sendYesTroubleshootingMessage(){
        DispatchQueue.main.async {
            let vcu = ViewControllerUtils()
            vcu.startActivityIndicatorAnimating(uiView: self.view)
            
            DispatchQueue.main.async {
                
                let mailSubjectTemplate = self.resourceMap!["fault_resolved_notify_title"]
                let mailContentTemplate = self.resourceMap!["fault_resolved_notify_content"]
                
                let memberRepo = MemberRepository()
                let member = memberRepo.GetMember()
                
                
                let subject = mailSubjectTemplate!.replacingOccurrences(of: "{Product Category}", with: "安裝產品:" + self.ProductNameLabel.text!  , options: .literal, range: nil)
                    .replacingOccurrences(of: "{Account}", with: "帳號:" + member!.loginId!, options: .literal, range: nil)
                
                let content = mailContentTemplate!.replacingOccurrences(of: "{Product Category}", with: "安裝產品:" + member!.faultReasonTypeText!, options: .literal, range: nil)
                    .replacingOccurrences(of: "{Account}", with:  "帳號:" + member!.loginId!, options: .literal, range: nil)
                    .replacingOccurrences(of: "{Error Reason}", with:  "故障原因:" + member!.faultReasonRemark!, options: .literal, range: nil)
                
                
                
                
                
                let notificationManager = NotificationManager()
                notificationManager.SendLineMessage(value1: content, value2: "", value3: "")
                
                notificationManager.SendMail(subject: subject, content: content)
                
                
                memberRepo.UpdateFaultReason(reasonType: -1, reasonTypeText: "", reasonRemark: "")
                
                self.finishNotifity(handler: { action in
                    self.goToMain()
                })
               
                
                DispatchQueue.main.async {
                    
                    vcu.stopActivityIndicatorAnimating()
                }
                
            }
        }
    }
    
    func goToMain(){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func finishNotifity(handler: ((UIAlertAction) -> Void)?){
        
        // 建立一個提示框
        let alertController = UIAlertController(
            title: "完成通知",
            message: "",
            preferredStyle: .alert)
        
        // 建立[確認]按鈕
        let okAction = UIAlertAction(
            title: "確認",
            style: .default,
            handler: handler)
        alertController.addAction(okAction)
        
        // 顯示提示框
        self.present(
            alertController,
            animated: true,
            completion: nil)
        
        
    }
    
}
