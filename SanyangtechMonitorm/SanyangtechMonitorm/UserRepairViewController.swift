//
//  ViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/1/31.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class UserRepairViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var ValidMessageLabel: UILabel!
    @IBOutlet weak var RepairButton: UIButton!
    @IBOutlet weak var RepairTypeTextField: UITextField!
    @IBOutlet weak var RepairContentTextView: UITextView!
    let PLACEHOLDER: String = "請輸入報修內容..."
    var repairTypes: Dictionary<String, String>? = [:]
    var selectedRepairType: Int?
    var selectedRepairTypeText: String?
    var resourceMap: Dictionary<String, String>?
    var member: MemberInfoMO?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let manager = SangyangTechCloud();
        let memberRepo = MemberRepository()
        repairTypes = manager.GetFaultReasons()
        resourceMap = manager.GetResources()
        member = memberRepo.GetMember()
        RepairContentTextView.text = PLACEHOLDER
        RepairContentTextView.textColor = UIColor.lightGray
        RepairContentTextView.returnKeyType = .done
        RepairContentTextView.delegate = self
        
        createRepairTypePicker()
        createToolbar()
        
        RepairContentTextView.layer.cornerRadius = 5
        RepairButton.layer.cornerRadius = 5
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "登出", style: UIBarButtonItem.Style.done, target: self, action: #selector(logoutAction(sender:)))
        
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func logoutAction(sender: UIBarButtonItem!) {
        let memberRepo = MemberRepository()
        memberRepo.DeleteMember()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if RepairContentTextView.text == PLACEHOLDER
        {
            RepairContentTextView.text = ""
            RepairContentTextView.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"
        {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""
        {
            textView.text = PLACEHOLDER
            textView.textColor = UIColor.lightGray
        }
    }
    
    func createRepairTypePicker(){
        
        let repairTypePicker = UIPickerView()
        repairTypePicker.delegate = self
        RepairTypeTextField.inputView = repairTypePicker
    }
    
    func createToolbar(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        
        let doneButton = UIBarButtonItem(title: "確定", style: .plain, target: self,
                                         action: #selector(UserRepairViewController.dismissKeyboard))
        
        toolBar.setItems([flexible, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        RepairTypeTextField.inputAccessoryView = toolBar
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
        
    }
    
    @IBAction func Logout(sender: UIButton){
        
        let memberRepo = MemberRepository()
        memberRepo.DeleteMember()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
        self.present(viewController, animated: true, completion: nil)
        
        
    }
    
    
    
    @IBAction func SendRepair(sender: UIButton){
        
        DispatchQueue.main.async {
            let vcu = ViewControllerUtils()
            vcu.startActivityIndicatorAnimating(uiView: self.view)
            
            DispatchQueue.main.async {
                
                if(self.RepairTypeTextField.text == "" || self.RepairContentTextView.text == self.PLACEHOLDER) {
                    
                    self.ValidMessageLabel.text = "請選擇安裝產品及填寫故障原因。"
                    print(self.RepairTypeTextField.text as Any)
                    print(self.RepairContentTextView.text)
                }else{
                    
                    let mailSubjectTemplate = self.resourceMap!["booking_notify_title"]
                    let mailContentTemplate = self.resourceMap!["booking_notify_content"]
                    
                    
                    let subject = mailSubjectTemplate!.replacingOccurrences(of: "{Product Category}", with: "安裝產品:" + self.RepairTypeTextField.text!, options: .literal, range: nil)
                        .replacingOccurrences(of: "{Account}", with: "帳號:" + self.member!.loginId!, options: .literal, range: nil)
                    
                    let content = mailContentTemplate!.replacingOccurrences(of: "{Product Category}", with: "安裝產品:" + self.RepairTypeTextField.text!, options: .literal, range: nil)
                        .replacingOccurrences(of: "{Account}", with: "帳號:" + self.member!.loginId!, options: .literal, range: nil)
                        .replacingOccurrences(of: "{Error Reason}", with: "故障原因:" + self.RepairContentTextView.text, options: .literal, range: nil)
                    
                    let notificationManager = NotificationManager()
                    notificationManager.SendLineMessage(value1 : content, value2 : "", value3 : "")
                    
                    notificationManager.SendMail(subject: subject, content: content)
                    
                    let memberRepo = MemberRepository()
                    memberRepo.UpdateFaultReason(reasonType: self.selectedRepairType!, reasonTypeText: self.selectedRepairTypeText!, reasonRemark: self.RepairContentTextView.text!)
                    
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    //let troubleshootingViewController = storyBoard.instantiateViewController(withIdentifier: "Troubleshooting") as! TroubleshootingViewController
                    //self.present(troubleshootingViewController, animated: true, completion: nil)
                    
                    
                    let viewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
                    self.present(viewController, animated: true, completion: nil)
                    
                }
                
                
                DispatchQueue.main.async {
                    vcu.stopActivityIndicatorAnimating()
                }
            }
        }
        
        
        
    }
    
}

extension UserRepairViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (repairTypes?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return repairTypes![String(row)]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRepairType = row
        selectedRepairTypeText = repairTypes?[String(row)]
        RepairTypeTextField.text = selectedRepairTypeText
    }
}


