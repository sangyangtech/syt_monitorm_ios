//
//  ViewController.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/1/31.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var LoginTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var ValidMessageLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var appVersionLabel: UILabel!
    /*
     let scrollView: UIScrollView = {
     let scrollView = UIScrollView()
     scrollView.translatesAutoresizingMaskIntoConstraints = false;
     scrollView.backgroundColor = .white
     scrollView.layoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
     return scrollView
     }()
     
     let logoImageView: UIImageView = {
     let baseImage = UIImage.init(named: "logo_full")
     
     let templatedImage = baseImage?.withRenderingMode(.alwaysTemplate)
     let logoImageView = UIImageView(image: templatedImage)
     logoImageView.translatesAutoresizingMaskIntoConstraints = false
     
     //logoImageView.frame = CGRect(x: 300, y: 300, width: 100, height: 50)
     return logoImageView
     }()
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        appVersionLabel.text = "version " + ((Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String)!)
        
        
        LoginButton.layer.cornerRadius = 5;
        
        //view.tintColor = .black
        //scrollView.backgroundColor = .white
        //view.addSubview(scrollView)
        /*
         NSLayoutConstraint.activate(
         NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollView]|",
         options: [],
         metrics: nil,
         views: ["scrollView" : scrollView])
         )
         NSLayoutConstraint.activate(
         NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollView]|",
         options: [],
         metrics: nil,
         views: ["scrollView" : scrollView])
         )
         
         let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapTouch))
         scrollView.addGestureRecognizer(tapGestureRecognizer)
         */
        
        /*
         var constraints = [NSLayoutConstraint]()
         
         let nextButton: MDCRaisedButton = {
         let nextButton = MDCRaisedButton()
         //nextButton.translatesAutoresizingMaskIntoConstraints = false
         nextButton.setTitle("NEXT", for: .normal)
         nextButton.backgroundColor = UIColor.red
         nextButton.frame = CGRect(x: 100, y: 100, width: 100, height: 50)
         //nextButton.addTarget(self, action: #selector(didTapNext(sender:)), for: .touchUpInside)
         return nextButton
         }()
         
         
         self.view.addSubview(logoImageView)
         self.view.addSubview(nextButton)
         
         constraints.append(NSLayoutConstraint(item: logoImageView,
         attribute: .top,
         relatedBy: .equal,
         toItem: self.view,
         attribute: .top,
         multiplier: 1,
         constant: 50))
         constraints.append(NSLayoutConstraint(item: logoImageView,
         attribute: .centerX,
         relatedBy: .equal,
         toItem: self.view,
         attribute: .centerX,
         multiplier: 1,
         constant: 50))
         
         
         
         NSLayoutConstraint.activate(constraints)
         */
        
        
        
    }
    
    /*
     @objc func didTapTouch(sender: UIGestureRecognizer) {
     view.endEditing(true)
     }
     
     @objc func didTapNext(sender: Any) {
     self.dismiss(animated: true, completion: nil)
     }
     */
    
    @IBAction func Login(sender: UIButton){
        
        DispatchQueue.main.async {
            let vcu = ViewControllerUtils()
            vcu.startActivityIndicatorAnimating(uiView: self.view)
            
            DispatchQueue.main.async {
                let manager = SangyangTechCloud()
                let memberRepo = MemberRepository()
                let accountData: AccountDTO? = manager.GetAccountInfo(id: self.LoginTextField.text!, pwd: self.PasswordTextField.text!)
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                if(accountData == nil){
                    self.ValidMessageLabel.text = "您輸入的帳號或密碼不正確"
                    
                } else {
                    memberRepo.ResetMember(loginId: (accountData?.LoginId)!, password: self.PasswordTextField.text!, role: (accountData?.Role)!, warrantyDate: (accountData?.WarrantyDate)!)
                    
                    
                    if(accountData?.Role == "user")
                    {
                        let dateFormatter = DateFormatter()
                        let currentDateTime = Date()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        let warrantyPeriodDate = dateFormatter.date(from: (accountData?.WarrantyDate)!)
                        if(warrantyPeriodDate! < currentDateTime){
                            let adminViewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
                            
                            self.present(adminViewController, animated: true, completion: nil)
                        }else
                        {
                            //let userRepairViewController = storyBoard.instantiateViewController(withIdentifier: "UserRepair") as! UserRepairViewController
                            //self.present(userRepairViewController, animated: true, completion: nil)
                            
                            let adminViewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
                            
                            self.present(adminViewController, animated: true, completion: nil)
                            
                        }
                        
                    } else {
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let adminViewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigation") as! MainViewController
                        
                        self.present(adminViewController, animated: true, completion: nil)
                    }
                }
                
                DispatchQueue.main.async {
                    vcu.stopActivityIndicatorAnimating()
                }
                
            }
        }
    }
    
    @IBAction func  showMessage(sender: UIButton){
        
        let model = SangyangTechCloud()
        let resulta: AccountDTO = model.GetAccountInfo(id: "test", pwd: "123qwe")!
        
        let alertController = UIAlertController(title: "welcome to my first app", message: resulta.LoginId, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style:UIAlertAction.Style.default, handler:nil))
        
        
        
        present(alertController, animated: true, completion: nil)
        
        
        //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid
        
        let parameters = ["value1":"123","value2":"131313","value3":"131313"]
        
        //create the url with URL
        let url = URL(string: "https://maker.ifttt.com/trigger/testtrigger/with/key/gES3D7EWYMbYF0orKEeYKwFyX8VHIVfrtrpHR76ePf2")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
    }
    
}
