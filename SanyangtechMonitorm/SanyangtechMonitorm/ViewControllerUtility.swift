//
//  ViewControllerUtility.swift
//  SanyangtechMonitorm
//
//  Created by CHUANG HUNG CHIEH on 2019/2/22.
//  Copyright © 2019 CHUANG HUNG CHIEH. All rights reserved.
//


import Foundation
import UIKit


class ViewControllerUtils {
    
 
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func startActivityIndicatorAnimating(uiView: UIView){
        activityIndicator.center = uiView.center
        activityIndicator.hidesWhenStopped = true;
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5);
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        uiView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        // UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func stopActivityIndicatorAnimating(){
        activityIndicator.stopAnimating()
        
        //UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    
}
